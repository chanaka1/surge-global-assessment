import { getValue } from "./lodash.helper";

const sendResponse = (responseStatus) => {
  let response = {};
  let meta = {
    status: responseStatus.status,
    message: responseStatus.message,
  };
  let error = {
    name: responseStatus.message,
  };

  return {
    setData: function (dataObject = {}) {
      response["data"] = dataObject;
      return this;
    },
    setMeta: function (metaObject = {}) {
      response["meta"] = {
        ...meta,
        ...metaObject,
      };
      return this;
    },
    setErrors: function (errorObject = {}) {
      response["errors"] = {
        ...error,
        ...errorObject,
      };
      return this;
    },
    setValidations: function (errorObject = {}) {
      response["errors"] = {
        ...error,
        ...{ details: errorObject },
      };
      return this;
    },
    get: function () {
      return response;
    },
  };
};

const getInputsForValidate = (formValue, key) => {
  let value = getValue(formValue, key, "");

  switch (typeof value) {
    case "string": {
      value = value.trim();
      break;
    }
    default: {
      break;
    }
  }
  return value;
};

const mapInputKey = (realInputKey, keyToMap) => {
  let arrayMatch = realInputKey.match(/(\.\d*\.)/g);
  let key = 0;
  let returnData = keyToMap.replace(/(\.\**\.)/g, (match) => {
    var value = arrayMatch[key];
    key++;
    return value;
  });

  return returnData;
};

const isNumber = (value) => {
  try {
    return value.toString().match(/^\d+$/);
  } catch (ex) {
    return false;
  }
};

const isEmptyValue = (value) => {
  if (
    value === null ||
    value === undefined ||
    value === "null" ||
    value === "undefined" ||
    value === ""
  ) {
    return true;
  } else {
    return false;
  }
};

export { sendResponse, mapInputKey, isEmptyValue, isNumber, getInputsForValidate };
