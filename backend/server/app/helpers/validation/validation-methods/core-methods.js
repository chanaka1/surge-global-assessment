import { getInputsForValidate, isEmptyValue } from "../../common.helper";

/**
 * @description validate required fields
 * @param {string} key input value key
 * @param {object} values form values
 * @param {array} param additional validation parameters
 * @param {string} message Error message
 * @param {object} filedList display name for form elements
 * @param {Function} cb callback function
 */
const required = (key, values, param, message, filedList, cb) => {
  try {

    let formValue = getInputsForValidate(values, key);
    if (isEmptyValue(formValue)) {
      cb(message)
    } else {
      cb(null, true);
    }

  } catch (ex) {
    console.log(
        `----------------Validation Exception At (required)-------------------`,
        `Input Key - ${key}`,
        `Exception - ${ex}`
    );

    cb(true);
  }
}

const same = (key, values, param, message, filedList, cb) => {
  try {

    const valueOne = getInputsForValidate(values, key);
    const valueTwo = getInputsForValidate(values, param[0]);

    if (valueOne !== valueTwo) {
      message = message.replace(":other", filedList[param[0]]);
      cb(message);
    } else {
      cb(null, true);
    }

  } catch (ex) {
    console.log(
        `----------------Validation Exception At (same)-------------------`,
        `Input Key - ${key}`,
        `Exception - ${ex}`
    );
  }

}

/**
 * @description validate email address
 * @param {string} key input value key
 * @param {object} values form values
 * @param {array} param additional validation parameters
 * @param {string} message Error message
 * @param {object} filedList display name for form elements
 * @param {Function} cb callback function
 */
const email = (key, values, param, message, filedList, cb) => {
  try {
    let formValue = getInputsForValidate(values, key);

    if (!formValue) {
      cb(null, true);
    } else if (formValue && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(formValue)) {
      cb(null, true);
    } else {
      cb(message);
    }

  } catch (ex) {
    console.log(
        `----------------Validation Exception At (required)-------------------`,
        `Input Key - ${key}`,
        `Exception - ${ex}`
    );

    cb(true);
  }
}

/**
 * @description validate max
 * @param {string} key input value key
 * @param {object} values form values
 * @param {array} param additional validation parameters
 * @param {string} message Error message
 * @param {object} filedList display name for form elements
 * @param {Function} cb callback function
 */
const max = (key, values, param, message, filedList, cb) => {
  try {
    const formValue = getInputsForValidate(values, key);
    if (formValue && formValue.length > param) {
      cb(message);
    } else {
      cb(null, true)
    }

  } catch (ex) {
    console.log(
        `----------------Validation Exception At (max)-------------------`,
        `Input Key - ${key}`,
        `Exception - ${ex}`
    );

    cb(true);
  }
}

/**
 * @description validate min
 * @param {string} key input value key
 * @param {object} values form values
 * @param {array} param additional validation parameters
 * @param {string} message Error message
 * @param {object} filedList display name for form elements
 * @param {Function} cb callback function
 */
const min = (key, values, param, message, filedList, cb) => {
  try {
    const formValue = getInputsForValidate(values, key);
    if (formValue && formValue.length < param) {
      cb(message);
    } else {
      cb(null, true)
    }

  } catch (ex) {
    console.log(
        `----------------Validation Exception At (min)-------------------`,
        `Input Key - ${key}`,
        `Exception - ${ex}`
    );

    cb(true);
  }
}

export {
  required,
  same,
  email,
  max,
  min,
}