import mongoose from "mongoose";

import { getValue } from "../../lodash.helper";
import { getInputsForValidate } from "../../common.helper";

const unique = (key, values, param, message, filedList, cb) => {
  try {
    let formValue = getInputsForValidate(values, key);
    const dbObj = mongoose.connection.db;

    let filterOption = {
      [getValue(param, "1", key)]: formValue,
    };

    if (mongoose.Types.ObjectId.isValid(getValue(param, "2", "-"))) {
      filterOption["_id"] = {
        $ne: mongoose.Types.ObjectId(getValue(param, "2", "-")),
      };
    }

    dbObj
      .collection(getValue(param, "0", key))
      .findOne(filterOption, function (error, result) {
        if (result) {
          cb(message, null);
        } else {
          cb(null, true);
        }
      });
  } catch (ex) {
    console.log(
      `----------------Validation Exception At (required)-------------------`,
      `Input Key - ${key}`,
      `Exception - ${ex}`
    );

    cb(true);
  }
};

export { unique };
