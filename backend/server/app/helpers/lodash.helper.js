import { pick, get, filter, findKey, uniqBy, isObject } from "lodash";

const pickByObject = (object, extract) => {
  return pick(object, extract);
};

const getValue = (object, path, defaultValue) => {
  return get(object, path, defaultValue);
};

const filterBy = (collection, filterFunction) => {
  return filter(collection, filterFunction);
};

const findByKey = (dataObject, searchBy) => {
  return findKey(dataObject, searchBy);
};

const uniq = (dataObject, uniq) => {
  return uniqBy(dataObject, uniq);
};

const isValidObject = (data) => {
  return isObject(data);
};

export { pickByObject, getValue, filterBy, findByKey, uniq, isValidObject };
