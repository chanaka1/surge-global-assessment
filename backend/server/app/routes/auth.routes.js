import express from "express";

import { token } from "../modules/auth/auth.controller";
import loginValidate from "../modules/auth/auth.validations";

const authRoutes = express.Router();

authRoutes.route("/").post(loginValidate, token);

export default authRoutes;
