import express from "express";

import {
  create,
  update,
  list,
  getById,
  remove,
} from "../modules/user/user.controller";
import authGuard from "../middlewares/auth.middleware";
import {
  userCreateValidate,
  userEditValidate,
} from "../modules/user/user.validations";

const userRoutes = express.Router();

userRoutes.route("/").post(userCreateValidate, create).get(authGuard, list);
userRoutes
  .route("/:id")
  .put(authGuard, userEditValidate, update)
  .get(authGuard, getById)
  .delete(authGuard, remove);

export default userRoutes;
