import express from "express";

import {
  create,
  update,
  list,
  getById,
  remove,
  updateStatus
} from "../modules/to-do/to-do.controller";
import authGuard from "../middlewares/auth.middleware";
import { toDoCreateEditValidate, toDoStatusUpdateValidate } from "../modules/to-do/to-do.validations";

const toDoRoutes = express.Router();

toDoRoutes.route("/").post(authGuard, toDoCreateEditValidate, create);
toDoRoutes.route("/list").get(authGuard, list);
toDoRoutes.route("/status/:id").patch(authGuard, toDoStatusUpdateValidate, updateStatus);
toDoRoutes
  .route("/:id")
  .put(authGuard, toDoCreateEditValidate, update)
  .get(authGuard, getById)
  .delete(authGuard, remove);

export default toDoRoutes;
