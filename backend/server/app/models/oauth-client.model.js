import mongoose from "mongoose";

const oauthClientSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
  },
  client_code: {
    type: String,
    trim: true,
    unique: "Client code already exists",
  },
  secret: {
    type: String,
    trim: true,
    unique: "Client secret already exists",
  },
  revoked: {
    type: Boolean,
    default: false,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: Date,
});

export default mongoose.model("OauthClient", oauthClientSchema);
