import mongoose from "mongoose";

const oauthRefreshTokenSchema = new mongoose.Schema({
  revoked: {
    type: Boolean,
    default: false,
  },
  expires_at: Date,
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: Date,
});

const oauthAccessTokenSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
  },
  client: {
    type: mongoose.Schema.ObjectId,
    ref: "OauthClient",
  },
  revoked: {
    type: Boolean,
    default: false,
  },
  expires_at: Date,
  oauth_refresh_token: oauthRefreshTokenSchema,
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: Date,
});

export default mongoose.model("OauthAccessToken", oauthAccessTokenSchema);
