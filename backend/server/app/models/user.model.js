import mongoose from "mongoose";
import bcrypt from "bcrypt";

const userSchema = mongoose.Schema({
  email: {
    type: String,
    trim: true,
    required: "Email is required",
    unique: true,
  },
  password: {
    type: String,
    required: "Password is required",
  },
  full_name: {
    type: String,
    trim: true,
    required: "Full name is required",
  },
  username: {
    type: String,
    trim: true,
    required: "Username is required",
    unique: true,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: Date,
});

userSchema
  .virtual("encrypted_password")
  .set(function (password) {
    this.password = this.encryptPassword(password);
  })
  .get(function () {
    return this.password;
  });

userSchema.methods = {
  authenticate: function (plainText) {
    return bcrypt.compareSync(plainText, this.password);
  },
  encryptPassword: function (password) {
    if (!password) throw new Error();
    try {
      return bcrypt.hashSync(password, 10);
    } catch (e) {
      throw new Error();
    }
  },
};

export default mongoose.model("User", userSchema);
