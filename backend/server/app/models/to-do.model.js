import mongoose from "mongoose";

const toDoSchema = mongoose.Schema({
  title: {
    type: String,
    required: "Name is required",
  },
  description: {
    type: String,
    required: "Description is required",
  },
  date_time: {
    type: Date,
    required: "Date & Time is required",
  },
  status: {
    type: String,
    enum: ["TO_DO", "IN_PROGRESS", "DONE"],
    required: "Status is required",
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: {
    type: Date,
    default: null,
  },
  deleted_at: {
    type: Date,
    default: null,
  },
  created_by: String,
  updated_by: {
    type: String,
    default: null,
  },
  deleted_by: {
    type: String,
    default: null,
  },
});

export default mongoose.model("ToDo", toDoSchema);
