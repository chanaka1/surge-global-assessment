import { unauthorizedResponse } from "../../config/apiResponse.config";
import { sendResponse } from "../helpers/common.helper";
import { getValue } from "../helpers/lodash.helper";

const permissionGuard = (permissions) => {
  return (req, res, next) => {
    let isAllowed = true;
    const userPermissons = getValue(req, "authUser.permissions", []);

    for (const value of permissions) {
      if (!userPermissons.includes(value)) {
        isAllowed = false;
        break;
      }
    }

    if (!isAllowed) {
      res
        .status(403)
        .send(sendResponse(unauthorizedResponse).setErrors({}).get());
    }

    next();
  };
};

export default permissionGuard;
