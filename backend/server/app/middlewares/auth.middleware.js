import jwt from "jsonwebtoken";

import { jwtSecret } from "../../config/core.config";
import { invalidTokenResponse } from "../../config/apiResponse.config";
import { sendResponse } from "../helpers/common.helper";
import OAuthAccessTokenRepository from "../repositories/OAuthAccessTokenRepository";

const authGuard = async (req, res, next) => {
  let token = req.headers["authorization"];

  if (!token || !token.startsWith("Bearer ")) {
    res
      .status(invalidTokenResponse.httpStatus)
      .send(sendResponse(invalidTokenResponse).setErrors({}).get());
  }

  token = token.slice(7, token.length);

  if (!token) {
    res
      .status(invalidTokenResponse.httpStatus)
      .send(sendResponse(invalidTokenResponse).setErrors({}).get());
  }

  try {
    const decoded = jwt.verify(token, jwtSecret);
    const accessTokenData = await OAuthAccessTokenRepository.isValidAccessToken(
      decoded["token"],
      decoded["client"]
    );

    if (accessTokenData["client"]) {
      req["authUser"] = accessTokenData["user"]["_id"];
      next();
    } else {
      res
        .status(invalidTokenResponse.httpStatus)
        .send(sendResponse(invalidTokenResponse).setErrors({}).get());
    }
  } catch (error) {
    res
      .status(invalidTokenResponse.httpStatus)
      .send(sendResponse(invalidTokenResponse).setErrors({}).get());
  }
};

export default authGuard;
