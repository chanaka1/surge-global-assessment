import {
  createToDo,
  updateToDo,
  listToDos,
  getToDoById,
  deleteToDo
} from "./to-do.service";

const create = (req, res) => {
  const createObj = {
    title: req.body.title,
    description: req.body.description,
    date_time: req.body.date_time,
    status: "TO_DO",
    created_by: req.authUser
  };

  createToDo(createObj, res);
};

const update = (req, res) => {
  const updateObj = {
    title: req.body.title,
    description: req.body.description,
    date_time: req.body.date_time,
    updated_by: req.authUser,
    updated_at: Date.now(),
  };

  updateToDo(updateObj, req.params.id, res);
};

const list = (req, res) => {
  listToDos(req.authUser, res);
};

const getById = (req, res) => {
  getToDoById(req.params.id, res);
};

const remove = (req, res) => {
  deleteToDo(req.params.id, req.authUser, res);
};

const updateStatus = (req, res) => {
  const updateObj = {
    status: req.body.status,
    updated_by: req.authUser,
    updated_at: Date.now(),
  };

  updateToDo(updateObj, req.params.id, res);
};

export { create, update, list, getById, remove, updateStatus };
