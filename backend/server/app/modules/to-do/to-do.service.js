import ToDoRepository from "../../repositories/ToDoRepository";
import {
  successCreateEditResponse,
  exceptionOccurred,
  successGetResponse,
  notFound,
} from "../../../config/apiResponse.config";
import { sendResponse } from "../../helpers/common.helper";

const createToDo = async (createObj, response) => {
  try {
    const result = await ToDoRepository.createData(createObj);

    response
      .status(successCreateEditResponse.httpStatus)
      .send(sendResponse(successCreateEditResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const updateToDo = async (updateObj, id, response) => {
  try {
    const result = await ToDoRepository.update(id, updateObj);

    response
      .status(successCreateEditResponse.httpStatus)
      .send(sendResponse(successCreateEditResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const listToDos = async (user, response) => {
  try {
    const result = await ToDoRepository.getToDoList(user);

    response
      .status(successGetResponse.httpStatus)
      .send(sendResponse(successGetResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const getToDoById = async (id, response) => {
  try {
    const result = await ToDoRepository.findByID(id);

    if (!result) {
      response
        .status(notFound.httpStatus)
        .send(sendResponse(notFound).setErrors({ name: "Not found" }).get());
    }

    result.password = undefined;
    
    response
      .status(successGetResponse.httpStatus)
      .send(sendResponse(successGetResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const deleteToDo = async (id, user, response) => {
  try {
    const result = await ToDoRepository.findByID(id);

    if (!result) {
      response
        .status(notFound.httpStatus)
        .send(sendResponse(notFound).setErrors({ name: "Not found" }).get());
    }

    const updateObj = {
      deleted_at: new Date(),
      deleted_by: user,
    };

    await ToDoRepository.update(id, updateObj);
    response.status(204).send();
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

export { createToDo, updateToDo, listToDos, getToDoById, deleteToDo };
