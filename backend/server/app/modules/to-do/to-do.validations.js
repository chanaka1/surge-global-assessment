import validate from "../../helpers/validation/index";
import { badResponse } from "../../../config/apiResponse.config";
import { getValue } from "../../helpers/lodash.helper";
import { sendResponse } from "../../helpers/common.helper";

const toDoCreateEditValidate = (req, res, next) => {
  const formData = {
    title: getValue(req.body, "title", ""),
    description: getValue(req.body, "description", ""),
    date_time: getValue(req.body, "date_time", ""),
  };

  validate(formData)
    .setFileds({
      title: "Title",
      description: "Description",
      date_time: "Date & Time",
    })
    .setRules({
      title: "required",
      description: "required",
      date_time: "required",
    })
    .setMessage({})
    .run((error, result) => {
      if (error) {
        return res
          .status(badResponse.httpStatus)
          .send(sendResponse(badResponse).setValidations(error).get());
      } else {
        req.body = formData;
        next();
      }
    });
};

const toDoStatusUpdateValidate = (req, res, next) => {
  const formData = {
    status: getValue(req.body, "status", "")
  };

  validate(formData)
    .setFileds({
      status: "Status"
    })
    .setRules({
      status: "required",
    })
    .setMessage({})
    .run((error, result) => {
      if (error) {
        return res
          .status(badResponse.httpStatus)
          .send(sendResponse(badResponse).setValidations(error).get());
      } else {
        req.body = formData;
        next();
      }
    });
};

export {
  toDoCreateEditValidate,
  toDoStatusUpdateValidate
};
