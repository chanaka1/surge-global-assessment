import jwt from "jsonwebtoken";

import OAuthAccessTokenRepository from "../../repositories/OAuthAccessTokenRepository";
import OAuthClientRepository from "../../repositories/OAuthClientRepository";
import UserRepository from "../../repositories/UserRepository";
import { unauthorizedResponse, successGetResponse } from "../../../config/apiResponse.config";
import { sendResponse } from "../../helpers/common.helper";
import { jwtSecret } from "../../../config/core.config";

const generateTokens = async (apiObj, response) => {
  try {
    const user = await UserRepository.validateUser(
      apiObj.username,
      apiObj.password
    );

    const client = await OAuthClientRepository.isValidClient(
      apiObj.client_id,
      apiObj.client_secret
    );

    const accessTokenExpiresAt = new Date();
    accessTokenExpiresAt.setHours(accessTokenExpiresAt.getHours() + 1);

    const refreshTokenExpiresAt = new Date();
    refreshTokenExpiresAt.setHours(refreshTokenExpiresAt.getHours() + 2);

    const accessTokenCreateObj = {
      user: user._id,
      client: client._id,
      expires_at: accessTokenExpiresAt,
      oauth_refresh_token: {
        expires_at: refreshTokenExpiresAt,
      },
    };

    const tokens = await OAuthAccessTokenRepository.createData(
      accessTokenCreateObj
    );

    const apiResponse = {
      access_token: jwt.sign(
        {
          token: tokens._id,
          client: client._id,
          sub: user._id,
        },
        jwtSecret,
        { expiresIn: "1h" }
      ),
      refresh_token: jwt.sign(
        {
          token: tokens["oauth_refresh_token"]["_id"],
        },
        jwtSecret,
        { expiresIn: "2h" }
      ),
      token_type: "Bearer",
      expiresIn: 3600,
    };

    response
      .status(successGetResponse.httpStatus)
      .send(apiResponse);
  } catch (error) {
    response
      .status(unauthorizedResponse.httpStatus)
      .send(sendResponse(unauthorizedResponse).setErrors(error).get());
  }
};

export { generateTokens };
