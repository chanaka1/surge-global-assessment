import validate from "../../helpers/validation/index";
import { badResponse } from "../../../config/apiResponse.config";
import { getValue } from "../../helpers/lodash.helper";
import { sendResponse } from "../../helpers/common.helper";

const loginValidate = (req, res, next) => {
  const formData = {
    username: getValue(req.body, "username", ""),
    password: getValue(req.body, "password", ""),
    client_id: getValue(req.body, "client_id", ""),
    client_secret: getValue(req.body, "client_secret", ""),
  };

  validate(formData)
    .setFileds({
      username: "Username",
      password: "Password",
      client_id: "Client ID",
      client_secret: "Client secret",
    })
    .setRules({
      username: "required",
      password: "required",
      client_id: "required",
      client_secret: "required",
    })
    .setMessage({})
    .run((error, result) => {
      if (error) {
        return res
          .status(badResponse.httpStatus)
          .send(sendResponse(badResponse).setValidations(error).get());
      } else {
        req.body = formData;
        next();
      }
    });
};

export default loginValidate;
