import { generateTokens } from "./auth.service";

const token = (req, res) => {
    const apiObj = {
        username: req.body.username,
        password: req.body.password,
        client_id: req.body.client_id,
        client_secret: req.body.client_secret,
    };

    generateTokens(apiObj, res);
};

export {
    token
}