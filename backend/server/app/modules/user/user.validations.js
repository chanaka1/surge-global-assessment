import validate from "../../helpers/validation/index";
import { badResponse } from "../../../config/apiResponse.config";
import { getValue } from "../../helpers/lodash.helper";
import { sendResponse } from "../../helpers/common.helper";

const userCreateValidate = (req, res, next) => {
  const formData = {
    username: getValue(req.body, "username", ""),
    password: getValue(req.body, "password", ""),
    email: getValue(req.body, "email", ""),
    full_name: getValue(req.body, "full_name", ""),
  };

  validate(formData)
    .setFileds({
      username: "Username",
      password: "Password",
      email: "Email",
      full_name: "Full Name",
    })
    .setRules({
      username: `required|unique:users,username,${getValue(req.params, "id", "-")}`,
      password: "required",
      email: `required|unique:users,email,${getValue(req.params, "id", "-")}`,
      full_name: "required",
    })
    .setMessage({})
    .run((error, result) => {
      if (error) {
        return res
          .status(badResponse.httpStatus)
          .send(sendResponse(badResponse).setValidations(error).get());
      } else {
        req.body = formData;
        next();
      }
    });
};

const userEditValidate = (req, res, next) => {
  const formData = {
    username: getValue(req.body, "username", ""),
    email: getValue(req.body, "email", ""),
    full_name: getValue(req.body, "full_name", ""),
  };

  validate(formData)
    .setFileds({
      username: "Username",
      email: "Email",
      full_name: "Full Name",
    })
    .setRules({
      username: `required|unique:users,username,${getValue(req.params, "id", "-")}`,
      email: `required|unique:users,email,${getValue(req.params, "id", "-")}`,
      full_name: "required",
    })
    .setMessage({})
    .run((error, result) => {
      if (error) {
        return res
          .status(badResponse.httpStatus)
          .send(sendResponse(badResponse).setValidations(error).get());
      } else {
        req.body = formData;
        next();
      }
    });
};

export {
  userCreateValidate,
  userEditValidate
};
