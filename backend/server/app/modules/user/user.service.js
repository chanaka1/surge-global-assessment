import UserRepository from "../../repositories/UserRepository";
import {
  successCreateEditResponse,
  exceptionOccurred,
  successGetResponse,
  notFound,
} from "../../../config/apiResponse.config";
import { sendResponse } from "../../helpers/common.helper";

const createUser = async (createObj, response) => {
  try {
    const result = await UserRepository.createData(createObj);
    result.password = undefined;

    response
      .status(successCreateEditResponse.httpStatus)
      .send(sendResponse(successCreateEditResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const updateUser = async (updateObj, id, response) => {
  try {
    const result = await UserRepository.update(id, updateObj);
    result.password = undefined;

    response
      .status(successCreateEditResponse.httpStatus)
      .send(sendResponse(successCreateEditResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const listUsers = async (response) => {
  try {
    const result = await UserRepository.list();

    response
      .status(successGetResponse.httpStatus)
      .send(sendResponse(successGetResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const getUserById = async (id, response) => {
  try {
    const result = await UserRepository.findByID(id);

    if (!result) {
      response
        .status(notFound.httpStatus)
        .send(sendResponse(notFound).setErrors({ name: "Not found" }).get());
    }

    result.password = undefined;
    
    response
      .status(successGetResponse.httpStatus)
      .send(sendResponse(successGetResponse).setData(result).get());
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

const deleteUser = async (id, response) => {
  try {
    const result = await UserRepository.findByID(id);

    if (!result) {
      response
        .status(notFound.httpStatus)
        .send(sendResponse(notFound).setErrors({ name: "Not found" }).get());
    }

    await UserRepository.delete(id);
    response.status(204).send();
  } catch (error) {
    response
      .status(exceptionOccurred.httpStatus)
      .send(sendResponse(exceptionOccurred).setErrors(error).get());
  }
};

export { createUser, updateUser, listUsers, getUserById, deleteUser };
