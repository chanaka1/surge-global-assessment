import {
  createUser,
  updateUser,
  listUsers,
  getUserById,
  deleteUser,
} from "./user.service";

const create = (req, res) => {
  const createObj = {
    email: req.body.email,
    encrypted_password: req.body.password,
    full_name: req.body.full_name,
    username: req.body.username,
  };

  createUser(createObj, res);
};

const update = (req, res) => {
  const updateObj = {
    email: req.body.email,
    full_name: req.body.full_name,
    username: req.body.username,
    updated_at: Date.now(),
  };

  updateUser(updateObj, req.params.id, res);
};

const list = (req, res) => {
  listUsers(res);
};

const getById = (req, res) => {
  getUserById(req.params.id, res);
};

const remove = (req, res) => {
  deleteUser(req.params.id, res);
};

export { create, update, list, getById, remove };
