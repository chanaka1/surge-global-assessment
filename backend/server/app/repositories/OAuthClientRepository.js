import oAuthClientModel from "../models/oauth-client.model";
import BaseRepository from "./BaseRepository";

class OAuthClientRepository extends BaseRepository {
  constructor() {
    super(oAuthClientModel);
  }

  isValidClient(id, secret) {
    return new Promise((resolve, reject) => {
      oAuthClientModel
        .findOne({ _id: id, secret, revoked: false })
        .exec((error, client) => {
          if (error || !client) {
            reject({ name: "Unauthorized" });
          } else {
            resolve(client);
          }
        });
    });
  }
}

export default new OAuthClientRepository();
