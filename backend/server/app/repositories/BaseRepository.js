class BaseRepository {
  constructor(model) {
    this.model = model;
  }

  createData(data) {
    return new Promise((resolve, reject) => {
      this.model.create(data, (err, data) => {
        if (err) {
          reject(err);
        }

        resolve(data);
      });
    });
  }

  list() {
    return new Promise((resolve, reject) => {
      this.model.find((err, data) => {
        if (err) {
          reject(err);
        }

        resolve(data);
      });
    });
  }

  findByID(id) {
    return new Promise((resolve, reject) => {
      this.model.findById(id, (err, data) => {
        if (err) {
          reject(err);
        }

        resolve(data);
      });
    });
  }

  findByAny(obj) {
    return new Promise((resolve, reject) => {
      this.model.find(obj, (err, data) => {
        if (err) {
          reject(err);
        }

        resolve(data);
      });
    });
  }

  update(id, obj) {
    return new Promise((resolve, reject) => {
      this.findByID(id)
        .then((result) => {
          if (!result) {
            reject({ name: "Not found" });
          }

          let existingData = result;
          let updatedOne = Object.assign(existingData, obj);

          updatedOne.save((err, data) => {
            if (err) {
              reject(err);
            }

            resolve(data);
          });
        })
        .catch((err) => reject(err));
    });
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      this.model.deleteOne({ _id: id }, (err, data) => {
        if (err) {
          reject(err);
        }

        resolve(data);
      });
    });
  }
}

export default BaseRepository;
