import oAuthAccessTokenModel from "../models/oauth-access-token.model";
import BaseRepository from "./BaseRepository";

class OAuthAccessTokenRepository extends BaseRepository {
  constructor() {
    super(oAuthAccessTokenModel);
  }

  isValidAccessToken(id, client) {
    return new Promise((resolve, reject) => {
      oAuthAccessTokenModel
        .findOne({
          _id: id,
          client: client,
          expires_at: { $gte: new Date() },
          revoked: false,
        })
        .populate({
          path: "client",
          model: "OauthClient",
          match: { revoked: false },
        })
        .populate({
          path: "user",
          model: "User"
        })
        .exec((error, result) => {
          if (error || !result) {
            reject({ name: "Unauthorized" });
          }

          resolve(result);
        });
    });
  }
}

export default new OAuthAccessTokenRepository();
