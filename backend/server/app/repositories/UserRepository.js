import BaseRepository from "./BaseRepository";
import userModel from "../models/user.model";

class UserRepository extends BaseRepository {
  constructor() {
    super(userModel);
  }

  validateUser(username, password) {
    return new Promise((resolve, reject) => {
      userModel
        .findOne({ username })
        .exec((error, user) => {
          if (error || !user || !user.authenticate(password)) {
            reject({ name: "Unauthorized" });
          } else {
            resolve(user);
          }
        });
    });
  }
}

export default new UserRepository();
