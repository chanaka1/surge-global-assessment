import toDoModel from "../models/to-do.model";
import BaseRepository from "./BaseRepository";

class ToDoRepository extends BaseRepository {
  constructor() {
    super(toDoModel);
  }

  getToDoList(user) {
    return new Promise((resolve, reject) => {
      toDoModel
        .find({ created_by: user, deleted_at: null, deleted_by: null })
        .sort({ date_time: "desc", created_at: "desc" })
        .exec((error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        });
    });
  }
}

export default new ToDoRepository();
