import app from "./express-config";
import userRoutes from "../app/routes/user.routes";
import authRoutes from "../app/routes/auth.routes";
import toDoRoutes from "../app/routes/to-do.routes";

// User routes
app.use(`/api/users`, userRoutes);

// Auth routes
app.use(`/api/auth`, authRoutes);

// Auth routes
app.use(`/api/to-dos`, toDoRoutes);

// Web route
app.get("/", (req, res) => {
  res.status(200).send("Surge Global");
});

export default app;
