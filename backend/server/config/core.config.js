const port = process.env.APP_PORT || 5000;
const jwtSecret = process.env.JWT_SECRET;

export {
    port,
    jwtSecret
};
