const successGetResponse = {
  status: "success",
  message: "DATA_RECEIVED",
  httpStatus: 200,
};

const successCreateEditResponse = {
  status: "success",
  message: "SUCCESSFULLY_CREATED",
  httpStatus: 201,
};

const successEditResponse = {
  status: "success",
  message: "SUCCESSFULLY_CREATED",
  httpStatus: 200,
};

const unauthorizedResponse = {
  status: "error",
  message: "Unauthorized",
  httpStatus: 401,
};

const badResponse = {
  status: "error",
  message: "Bad Request",
  httpStatus: 400,
};

const exceptionOccurred = {
  status: "error",
  message: "EXCEPTION_OCCURRED",
  httpStatus: 500,
};

const notFound = {
  status: "error",
  message: "NOT_FOUND",
  httpStatus: 404,
};

const invalidTokenResponse = {
  status: "error",
  message: "INVALID_TOKEN",
  httpStatus: 401,
};

export {
  successGetResponse,
  successCreateEditResponse,
  successEditResponse,
  unauthorizedResponse,
  exceptionOccurred,
  badResponse,
  notFound,
  invalidTokenResponse
};
