const dbConfig={
    connection: process.env.DB_CONNECTION || '',
    host: process.env.DB_HOST || "",
    port: process.env.DB_PORT || 3306,
    database: process.env.DB_DATABASE || "",
    userName: process.env.DB_USERNAME || "root",
    password: process.env.DB_PASSWORD || ""
};

const mongoUri = `mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.database}`;
const mongoAtlasUri = `mongodb+srv://chanaka:mysql123@cluster0.cdthf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;

export {
    dbConfig,
    mongoUri,
    mongoAtlasUri
};