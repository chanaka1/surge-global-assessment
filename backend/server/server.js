import mongoose from "mongoose";

import app from "./bootstrap/routes-config";
import { port } from "./config/core.config";
import { mongoAtlasUri } from "./config/database.config";

mongoose
  .connect(mongoAtlasUri)
  .then(() => {
    console.log("DB Connection has been established successfully");
  })
  .catch((err) => {
    console.log(`Unable to connect DB: ${err}`);
  });

app.listen(port, (err) => {
  console.log(`Server has started on ${port}`);
});
