import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";

import ErrorBoundary from "../components/ui-elements/ErrorBoundary";
import DisplayError from "../components/ui-elements/DisplayError";
import LoginRoutes from "../components/modules/login/Login.routes";
import RegisterRoutes from "../components/modules/register/Register.routes";
import TaskBoardRoutes from "../components/modules/Taskboard/TaskBoard.routes";

const Routes = () => {
  return (
    <BrowserRouter>
        <Switch>
            <ErrorBoundary displayComponent={DisplayError}>
                <LoginRoutes />
                <RegisterRoutes />
                <TaskBoardRoutes />
            </ErrorBoundary>
        </Switch>
    </BrowserRouter>
  );
};

export default Routes;
