import React from 'react';

import { UIContextProvider } from "../components/providers/context-providers/UIContext.provider";
import { AuthContextProvider } from "../components/providers/context-providers/AuthContext.provider";
import { FormContextProvider } from "../components/providers/context-providers/FormContext.provider";

const ProviderComposer = ({ contexts, children }) => {
  return contexts.reduceRight(
    (kids, parent) =>
      React.cloneElement(parent, {
        children: kids,
      }),
    children
  );
};

const ContextProvider = ({ children }) => {
  return (
    <ProviderComposer
      contexts={[       
        <UIContextProvider />,
        <AuthContextProvider />,
        <FormContextProvider />
      ]}
    >
      {children}
    </ProviderComposer>
  );
};

export {
  ContextProvider
};