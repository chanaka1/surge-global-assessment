import React from "react";

import { ContextProvider } from "./Providers";
import { MaterialThemeProvider } from "../components/ui-elements/BaseElements";
import Routes from "./Routes";

const App = () => {
    return (
        <ContextProvider>
            <MaterialThemeProvider>
                <Routes />
            </MaterialThemeProvider>
        </ContextProvider>
    );
};

export default App;