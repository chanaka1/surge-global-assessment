//core-methods
import {
    required, same, email, max, min
} from "./core-methods";

//custom-methods

export {
    //core-methods
    required,
    same,
    email,
    max,
    min,
    //custom-methods
};