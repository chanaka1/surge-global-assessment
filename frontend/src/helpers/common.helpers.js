import { _get, _unset } from "./lodash.wrappers";

/**
 * @description Get the type of given value
 * @param {*} value
 */
const getType = (value) => {
    if (value === null)
        return "undefined";

    return typeof (value);
};

/**
 * @description get from inputs for form validation
 * @param {Object|Array} formValue form data list
 * @param {string} key form value key
 */
const getInputsForValidate = (formValue, key) => {
    let value = _get(formValue, key, "");

    switch (typeof (value)) {
        case "string":
            {
                value = value.trim();
                break;
            }
        default:
            {
                break;
            }
    }

    return value;
}

/**
 * @description genarate map key for form validation
 * @param {string} realInputKey known key
 * @param {string} keyToMap key should find
 */
const mapInputKey = (realInputKey, keyToMap) => {
    let arrayMatch = realInputKey.match(/(\.\d*\.)/g);
    let key = 0;
    let returnData = keyToMap.replace(/(\.\**\.)/g, (match) => {
        var value = arrayMatch[key];
        key++;
        return value;
    });

    return returnData;
}

/**
 * @param (any) value
 */
const isEmptyValue = (value) => {
    if (value === null || value === undefined || value === "" || value === "null" || value === "undefined") {
        return true;
    } else {
        return false;
    }
}

/**
 * @param errors {array}
 * @param propertyName {string}
 * @param isErrorStatusOnly {boolean}
 * @returns {boolean|string|*}
 */
const getInputErrors = (errors = [], propertyName, isErrorStatusOnly = false) => {
    const checkError = errors.filter((element) => element.property === propertyName);

    if (checkError.length > 0) {
        return isErrorStatusOnly ? true : _get(checkError, "[0].message", "");
    } else {
        return isErrorStatusOnly ? false : "";
    }
};

/**
 * @description remove unnecessary keys from formObject
 * @param formObject
 * @returns {*}
 */
const getDataByFormObject = ({ ...formObject }) => {
    _unset(formObject, "_uibackProsess");
    _unset(formObject, "_formGroupLinkKey");
    _unset(formObject, "_uiFormGroup");
    _unset(formObject, "_uiFormDescription");
    _unset(formObject, "_updateStatus");
    _unset(formObject, "_errors");
    _unset(formObject, "_onLoad");

    return formObject;
};

export {
    getType,
    getInputsForValidate,
    mapInputKey,
    isEmptyValue,
    getInputErrors,
    getDataByFormObject
};
