import { authTokenStorageKey } from '../config/core.config';

/**
 * @description Set data to local storage
 * @param {String} key
 * @param {Object} contentArray
 * @returns {boolean}
 */
const setToLocalStorage=(key, contentArray)=>{
    try {
        localStorage.setItem(key, JSON.stringify(contentArray));
        return true;
    } catch (ex) {
        return false;
    }
};

/**
 * @description Set auth data to local storage
 * @param {Object} contentArray
 * @returns {boolean}
 */
const setAuthData=(contentArray)=>{
    return setToLocalStorage(authTokenStorageKey,contentArray);
};

/**
 * @description Get data from local storage
 * @param {String} key
 * @returns {boolean|any}
 */
const getFromLocalStorage=(key)=>{
    try {
        let data = localStorage.getItem(key);
        return JSON.parse(data);
    } catch (ex) {
        return false;
    }
};

/**
 * @description Remove data from local storage
 * @param {String} key
 * @returns {boolean}
 */
const reomveFromLocalStorage=(key)=>{
    try {
        localStorage.removeItem(key);
        return true;
    } catch (ex) {
        return false;
    }
};

/**
 * @description Remove auth data from local storage
 * @returns {boolean}
 */
const logoutUser=()=>{
    return reomveFromLocalStorage(authTokenStorageKey);
};

/**
 * @description Checked auth data in local storage
 * @returns {{status: boolean}|{result: (*|boolean), status: boolean}}
 */
const checkUserINLocalStorage=()=>{
    try {
        let data = getFromLocalStorage(authTokenStorageKey);
        if (data) {            
            return { status: true, result: data };
        } else {
            return { status: false };
        }
    } catch (ex) {
        return { status: false };
    }
};

export {
    setToLocalStorage,
    getFromLocalStorage,
    reomveFromLocalStorage,
    checkUserINLocalStorage,
    setAuthData,
    logoutUser
};