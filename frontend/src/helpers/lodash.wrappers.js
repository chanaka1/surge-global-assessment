import lodashGet from "lodash.get";
import lodashFindIndex from "lodash.findindex";
import lodashUnset from "lodash.unset";

/**
 * Lodash get() wrapper
 * @param {Object} object
 * @param {Array|string} path
 * @param {*} defaultValue
 */

const _get = (object, path, defaultValue) => {
    return lodashGet(object, path, defaultValue);
};

/**
 * lodash findindex() wrapper
 * @param {Object} object
 * @param {Array|Function} finder
 */

const _findIndex = (object, finder) => {
    return lodashFindIndex(object, finder);
};

/**
 * lodash unset() wrapper
 * @param {Object} object
 * @param {Array|string} path
 */

const _unset = (object, path) => {
    return lodashUnset(object, path);
};

export {
    _get,
    _findIndex,
    _unset
};