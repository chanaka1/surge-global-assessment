/**
 * --------------------------------------------
 * @Description: Base template colors
 * --------------------------------------------
 */
const baseTemplate={
    palette: {
        primary: {
            light: '#2196f3',
            main: '#2196f3',
            dark: '#2196f3',
        },
        secondary: {
            light: '#FFFFFF',
            main: '#FFFFFF',
            dark: '#2196f3',
        },
    }
};

export {
    baseTemplate
};