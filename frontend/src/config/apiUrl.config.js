import { apiBaseUrl } from "./core.config";

export const usersAPI = `${apiBaseUrl}api/users`;
export const authAPI = `${apiBaseUrl}api/auth`;
export const todoAPI = `${apiBaseUrl}api/to-dos`;