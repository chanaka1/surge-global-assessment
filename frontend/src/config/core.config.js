const authTokenStorageKey = "@todoAuthKey"; // Local storage storing key
const apiBaseUrl = process.env.REACT_APP_API_BASE_URL; // Backend API base url
const clientId = process.env.REACT_APP_CLIENT_ID;
const clientSecret = process.env.REACT_APP_CLIENT_SECRET;

export {
  authTokenStorageKey,
  apiBaseUrl,
  clientId,
  clientSecret
};
