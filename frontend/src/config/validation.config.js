const errorMessageList = {
    required: "The :attribute is required",
    max: "The :attribute may not be greater than :max",
    min: "The :attribute must be at least :min",
    email: "The :attribute must be a valid email address",
    same: "The :attribute and :other must match"
};

export {
    errorMessageList
};