//FormContext
export const initFormGroupKey="@FormContext/SET_INIT_FORM";
export const removeFormGroupKey="@FormContext/REMOVE_FORM_GROUP";
export const setErrorsKey="@FormContext/SET_INPUT_ERRORS";
export const setInputValueChangeKey="@FormContext/SET_FORM_INPUT_CHANGE";

//UIContext
export const setPageLoaderKey="@UIContext/SET_PAGE_LOADER";
export const setFlashMessageKey="@UIContext/SET_FLASH_MESSAGE";
export const removeFlashMessageKey="@UIContext/REMOVE_FLASH_MESSAGE";

//AuthContext
export const setAuthTokenKey="@AuthContext/SET_AUTH_TOKEN";
export const setUnauthorisedUserKey="@AuthContext/SET_UNAUTHORISED";
export const setAuthUserObjectKey="@AuthContext/SET_AUTH_USER_DATA";