import React, { createContext, useReducer } from 'react';

import { authAction } from '../context-helpers/authContext.helpers';
import {
  setAuthTokenKey,
  setUnauthorisedUserKey,
  setAuthUserObjectKey,
} from '../../../config/actionKeys.config';

const initialState = {
  accessToken: null,
  refreshToken: null,
  isAuthenticated: null,
  authUser: {},
};

const AuthContext = createContext({});

const authReducer = (state, action) => {
  switch (action.type) {
    case setAuthTokenKey:
      return {
        ...state,
        accessToken: action.payload.accessToken,
        refreshToken: action.payload.refreshToken,
        isAuthenticated: true,
        authUser: {},
      };
    case setUnauthorisedUserKey:
      return {
        ...initialState,
        isAuthenticated: false,
      };
    case setAuthUserObjectKey:
      return {
        ...state,
        authUser: {
          ...state.authUser,
          ...action.payload,
        },
      };
    default:
      return state;
  }
};

const AuthContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);
  const dispatchFunction = authAction(dispatch);

  return (
    <AuthContext.Provider value={[state, dispatchFunction]}>
      {children}
    </AuthContext.Provider>
  );
};

export {
  AuthContext,
  AuthContextProvider
};
