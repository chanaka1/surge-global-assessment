import React, { createContext, useReducer } from "react";

import { formActionFn } from "../context-helpers/formContext.helpers";
import {
    initFormGroupKey,removeFormGroupKey,setInputValueChangeKey, setErrorsKey
} from "../../../config/actionKeys.config";

const initialState = {}
const FormContext = createContext({});

const uiReducer=(state, action)=>{    
    switch (action.type) {
        case initFormGroupKey:
            return {
                ...state,
                ...action.payload
            };           
        case removeFormGroupKey:
            delete state[action.payload];
            return state; 
        case setInputValueChangeKey:   
            return {
                ...state,
                [action.formGroupKey]:{
                    ...state[action.formGroupKey],
                    [action.inputKey]:action.value,
                    _updateStatus:!state[action.formGroupKey]["_updateStatus"]
                }
            }; 
        case setErrorsKey:
            return {
                ...state,
                [action.formGroupKey]:{
                    ...state[action.formGroupKey],
                    _errors:action.payload,
                    _updateStatus:!state[action.formGroupKey]["_updateStatus"]
                }
            };
        default:
            return state;
    }
};

const FormContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer(uiReducer, initialState);
    const dispatchFn = formActionFn(dispatch);

    return(
        <FormContext.Provider value={[state, dispatchFn]}>
            {children}
        </FormContext.Provider>
    )
};

export {
    FormContext,
    FormContextProvider
};