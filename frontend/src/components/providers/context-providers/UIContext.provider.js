import React, { createContext, useReducer } from "react";

import { uiAction } from "../context-helpers/uiContext.helpers";
import { _findIndex } from "../../../helpers/lodash.wrappers";
import {
    setPageLoaderKey, setFlashMessageKey, removeFlashMessageKey
} from "../../../config/actionKeys.config";

const initialState={
    setPageLoader:false,
    currentRouteKey:"",
    flashMessageList:[]
};

const UIContext=createContext({});

const uiReducer=(state, action)=>{  
    switch (action.type) {
        case setPageLoaderKey:
            return { 
                ...state,
                setPageLoader: action.payload
            };
        case setFlashMessageKey:

            let currentList=state.flashMessageList||[];
            
            if((currentList).length >= 3){
                currentList=currentList.slice(1,currentList.length);
            }

            return {
                ...state,
                flashMessageList:[...currentList, action.payload]
            };
        case removeFlashMessageKey:

            const key=_findIndex(state.flashMessageList,["key",action.key]);

            if(key!==-1){
               return {
                ...state,
                flashMessageList:(state.flashMessageList||[]).slice(1,(state.flashMessageList||[]).length)
               };
            }

            break;
        default:
            return state;
    }
};

const UIContextProvider = ({ children }) => {
    const [state,dispatch] = useReducer(uiReducer,initialState);
    const dispatchFunction = uiAction(dispatch);

    return(
        <UIContext.Provider value={[state,dispatchFunction]}>
            {children}
        </UIContext.Provider>
    );
};

export {
    UIContext,
    UIContextProvider
};