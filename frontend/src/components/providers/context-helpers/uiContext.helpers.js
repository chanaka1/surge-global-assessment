import {
    setPageLoaderKey, setFlashMessageKey, removeFlashMessageKey
} from "../../../config/actionKeys.config";

/**
 * @description Set flash messages
 * @param {Function} dispatch
 * @param {Object} messageObject
 */
const setFlashMessage=(dispatch, messageObject)=>{
    const dateObject = new Date();
    const key = `${dateObject.getFullYear()}${(dateObject.getMonth() + 1).toString().padStart(2, '0')}${dateObject.getDate().toString().padStart(2, '0')}${dateObject.getHours().toString().padStart(2, '0')}${dateObject.getMinutes().toString().padStart(2, '0')}${dateObject.getSeconds().toString().padStart(2, '0')}`;

    dispatch({
        type: setFlashMessageKey,
        payload: {
            ...messageObject,
            key:key
        }
    });
    
    setTimeout(()=>{
        removeFlashMessage(dispatch,key);
    }, 5000);
};

/**
 * @description Remove flash messages
 * @param {Function} dispatch
 * @param {String} key
 */
const removeFlashMessage=(dispatch,key)=>{
    dispatch({
        type: removeFlashMessageKey,
        key: key
    });
};

/**
 * @description Set page loader
 * @param {Function} dispatch
 * @param {Boolean} status
 */
const setPageLoader=(dispatch,status)=>{
    dispatch({
        type: setPageLoaderKey,
        payload: status
    });
};

const uiAction=(dispatch)=>{
    return {
        setPageLoader:(status)=>setPageLoader(dispatch,status),
        setFlashMessage:(messageObject)=>setFlashMessage(dispatch,messageObject),
        removeFlashMessage:(key)=>removeFlashMessage(dispatch,key)
    };
};

export {
    uiAction
};