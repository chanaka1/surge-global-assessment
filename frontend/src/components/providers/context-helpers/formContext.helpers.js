import {
    initFormGroupKey,removeFormGroupKey,setInputValueChangeKey, setErrorsKey
} from "../../../config/actionKeys.config";

/**
 * @description Initialize form object
 * @param {Function} dispatch
 * @param {Object} object
 */
const initFromFn=(dispatch,object)=>{
    dispatch({
        type:initFormGroupKey,
        payload:object
    });
};

/**
 * @description Remove a form object
 * @param {Function} dispatch
 * @param formGroupKey
 */
const removeFromGroupFn=(dispatch,formGroupKey)=>{
    dispatch({
        type:removeFormGroupKey,
        payload:formGroupKey
    });
};

/**
 * @description Set validation errors
 * @param {Function} dispatch
 * @param {String} formGroupKey
 * @param {Array} errors
 */
const setFormErrorFn=(dispatch,formGroupKey,errors)=>{
    dispatch({
        type:setErrorsKey,
        formGroupKey:formGroupKey,
        payload:errors
    });
};

/**
 * @description Change value of a element in a form object
 * @param {Function} dispatch
 * @param {String} formGroupKey
 * @param {String} inputKey
 * @param {*} value
 */
const changeInputFn=(dispatch,formGroupKey,inputKey,value)=>{
    dispatch({
        type:setInputValueChangeKey,
        formGroupKey:formGroupKey,
        inputKey:inputKey,
        value:value
    });
};

const formActionFn=(dispatch)=>{
    return {    
        initFromFn:(object)=>initFromFn(dispatch,object),
        removeFromGroupFn:(formGroupKey)=>removeFromGroupFn(dispatch,formGroupKey),
        changeInputFn:(formGroupKey,inputKey,value)=>changeInputFn(dispatch,formGroupKey,inputKey,value),
        setFormErrorFn:(formGroupKey,errors)=>setFormErrorFn(dispatch,formGroupKey,errors)
    }
};

export {
    formActionFn
};
