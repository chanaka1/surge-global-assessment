import {
  setAuthTokenKey,
  setUnauthorisedUserKey,
  setAuthUserObjectKey,
} from '../../../config/actionKeys.config';
import {
  logoutUser,
} from '../../../helpers/manageStorage.helpers';

/**
 * @description Set token data
 * @param {Function} dispatch
 * @param {Object} tokenObject
 */
const setTokensFn = (dispatch, tokenObject) => {
  dispatch({
    type: setAuthTokenKey,
    payload: tokenObject,
  });
};

/**
 * @description Remove token data
 * @param {Function} dispatch
 */
const unauthorisedUserFn = (dispatch) => {
  logoutUser();

  dispatch({
    type: setUnauthorisedUserKey,
  });
};

/**
 * @description Set logged in user data
 * @param {Function} dispatch
 * @param {Object} userObject
 */
const setAuthUserFn = (dispatch, userObject) => {
  dispatch({
    type: setAuthUserObjectKey,
    payload: userObject,
  });
};

const authAction = (dispatch) => {
  return {
    setTokensFn: (tokenObject) => setTokensFn(dispatch, tokenObject),
    unauthorisedUserFn: () => unauthorisedUserFn(dispatch),
    setAuthUserFn: (userObject) => setAuthUserFn(dispatch, userObject)
  };
};

export {
  authAction
};
