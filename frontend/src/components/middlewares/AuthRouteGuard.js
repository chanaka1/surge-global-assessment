import React, { useEffect, useContext } from "react";
import { Route, Redirect } from "react-router-dom";

import { checkUserINLocalStorage } from "../../helpers/manageStorage.helpers";
import { AuthContext } from "../providers/context-providers/AuthContext.provider";

const AuthRouteGuard = ({
    component: Component,
    isAuthProtected,
    ...rest
}) => {
    const [authState, authAction] = useContext(AuthContext);

    useEffect(() => {
        if (authState.isAuthenticated === null) {
            const localStoreData = checkUserINLocalStorage();

            if (localStoreData.status === true) {
                authAction.setTokensFn(localStoreData.result);
            } else {
                authAction.unauthorisedUserFn();
            }
        }
    },[]);

    return (
        <Route
            {...rest}
            render={props => {
                if (isAuthProtected == true && authState.isAuthenticated == null) {
                    return (<div>loading</div>)
                } else if (isAuthProtected == true && authState.isAuthenticated == false) {
                    return(
                        <Redirect
                            to={{ pathname: "/login", state: { from: props.location } }}
                        />
                    )
                } else if (isAuthProtected == false && authState.isAuthenticated == true) {
                    return(
                        <Redirect
                            to={{ pathname: "/", state: { from: props.location } }}
                        />
                    )
                } else if (isAuthProtected == false || authState.isAuthenticated == true) {
                    return (
                        <Component {...props} />
                    );
                }
            }}
        />
    );
};

export default AuthRouteGuard;
