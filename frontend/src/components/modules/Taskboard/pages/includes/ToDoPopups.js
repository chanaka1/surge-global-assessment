import React, { useContext, useEffect, Fragment } from "react";

import { FormContext } from "../../../../providers/context-providers/FormContext.provider";
import { _get } from "../../../../../helpers/lodash.wrappers";
import { getInputErrors } from "../../../../../helpers/common.helpers";
import InputDateTimePicker from "../../../../ui-elements/InputDateTimePicker";
import InputField from "../../../../ui-elements/InputField";

const ViewToDoPopup = ({
    title = "",
    description = "",
    dateTime = "",
    status = "",
}) => {
    const getDateWithFormat = (value) => {
        const dateObject = new Date(value);
        return `${dateObject.getFullYear()}-${(dateObject.getMonth() + 1).toString().padStart(2, '0')}-${dateObject.getDate().toString().padStart(2, '0')} ${dateObject.getHours().toString().padStart(2, '0')}:${dateObject.getMinutes().toString().padStart(2, '0')}`;
    };

    const statuses = {
        TO_DO: "Todo",
        IN_PROGRESS: "In Progress",
        DONE: "Done"
    };

    return (
        <dl className={"row"}>
            <dt className={"col-sm-4 heading-style"}>Title</dt>
            <dd className={"col-sm-8 value-style"}>{title}</dd>

            <dt className={"col-sm-4 heading-style"}>Date & Time</dt>
            <dd className={"col-sm-8 value-style"}>{getDateWithFormat(dateTime)}</dd>

            <dt className={"col-sm-4 heading-style"}>Status</dt>
            <dd className={"col-sm-8 value-style"}>{statuses[status]}</dd>

            <dt className={"col-sm-4 heading-style"}>Description</dt>
            <dd className={"col-sm-8 value-style"}>{description}</dd>
        </dl>
    )
};

const EditToDoPopup = ({
    title = "",
    description = "",
    dateTime = "",
    status = "",
}) => {
    const [formState, formAction] = useContext(FormContext);
    const formKey = "edit_todo_form";

    useEffect(() => {
        formAction.initFromFn({
            [formKey]: {
                title: title,
                description: description,
                status: status,
                date_time: new Date(dateTime)
            }
        });

        return () => {
            formAction.removeFromGroupFn(formKey);
        };
    }, []);

    const onChangeFn = (event) => {
        formAction.changeInputFn(formKey, event.name, event.value);
    };

    return (
        <Fragment>
            <InputField
                inputName={"title"}
                isRequired={true}
                label={"Title"}
                inputValue={_get(formState, `${formKey}.title`, "")}
                isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "title", true)}
                errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "title")}
                onChangeFn={(event) => onChangeFn(event)}
            />

            <InputDateTimePicker
                inputName={"date_time"}
                isRequired={true}
                label={"Date & Time"}
                color={"primary"}
                helperText={"Please click the calendar icon"}
                inputValue={_get(formState, `${formKey}.date_time`, new Date())}
                isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "date_time", true)}
                errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "date_time")}
                onChangeFn={(event) => onChangeFn(event)}
            />

            <InputField
                inputName={"description"}
                isRequired={true}
                label={"Description"}
                isMultiline={true}
                rows={6}
                inputValue={_get(formState, `${formKey}.description`, "")}
                isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "description", true)}
                errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "description")}
                onChangeFn={(event) => onChangeFn(event)}
            />
        </Fragment>
    );
};

const DeleteToDoPopup = () => {
    return (
        <div className={"value-style"}>
            Are you sure want to delete this task?
        </div>
    );
};

export {
    ViewToDoPopup,
    EditToDoPopup,
    DeleteToDoPopup
}