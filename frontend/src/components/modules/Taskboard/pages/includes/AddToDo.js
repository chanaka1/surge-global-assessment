import React, { Fragment, useContext, useEffect } from "react";

import CustomInputField from "./CustomInputField";
import InputDateTimePicker from "../../../../ui-elements/InputDateTimePicker";
import InputButton from "../../../../ui-elements/InputButton";
import { FormContext } from "../../../../providers/context-providers/FormContext.provider";
import { UIContext } from "../../../../providers/context-providers/UIContext.provider";
import { getInputErrors } from "../../../../../helpers/common.helpers";
import { _get } from "../../../../../helpers/lodash.wrappers";
import validate from "../../../../../helpers/validation";
import { callApi } from "../../../../../helpers/callApi.helpers";
import { todoAPI } from "../../../../../config/apiUrl.config";

const AddToDo = (props) => {
    const [formState, formAction] = useContext(FormContext);
    const [, uiAction] = useContext(UIContext);
    const formKey = "add_todo_form";

    useEffect(() => {
        formAction.initFromFn({ [formKey]: { date_time: new Date() } });

        return () => {
            formAction.removeFromGroupFn(formKey);
        };
    }, []);

    const onChangeFn = (event) => {
        formAction.changeInputFn(formKey, event.name, event.value);
    };

    const onClickBtnFn = () => {
        let validationErrors = true;

        validate(_get(formState, formKey, {}))
            .setFields({
                "title": "Title",
                "description": "Description",
                "date_time": "Date & Time",
            })
            .setRules({
                "title": "required|max:50",
                "description": "required|max:255",
                "date_time": "required",
            })
            .run((result) => {
                if (result) {
                    formAction.setFormErrorFn(formKey, result);
                    validationErrors = true;
                } else {
                    formAction.setFormErrorFn(formKey, []);
                    validationErrors = false;
                }
            });

        if (!validationErrors) {
            uiAction.setPageLoader(true);

            callApi(todoAPI)
                .headers(true)
                .isMultipart(false)
                .method("post")
                .body({
                    "title": _get(formState, `${formKey}.title`, ""),
                    "description": _get(formState, `${formKey}.description`, ""),
                    "date_time": _get(formState, `${formKey}.date_time`, new Date()).toISOString(),
                    "status": "TO_DO"
                })
                .send((error, result) => {
                    if (error) {
                        uiAction.setPageLoader(false);

                        if (_get(error, "data.errors.name", "") === "Bad Request") {
                            formAction.setFormErrorFn(formKey, _get(error, "data.errors.details", []));
                        } else {
                            uiAction.setFlashMessage({
                                status: true,
                                message: "An error has occurred",
                                messageType: "error"
                            });
                        }
                    } else {
                        formAction.changeInputFn(formKey, "title", "");
                        formAction.changeInputFn(formKey, "description", "");
                        formAction.changeInputFn(formKey, "date_time", new Date());

                        props.getTodosFn();

                        uiAction.setFlashMessage({
                            status: true,
                            message: "Task has successfully created",
                            messageType: "success"
                        });
                    }
                });
        }
    };

    return (
        <Fragment>
            <div className={"mt-3 mb-4 add-todo-heading"}>
                <i className="far fa-edit"></i>
                <h5>Make a New Task</h5>
            </div>

            <CustomInputField
                inputName={"title"}
                isRequired={true}
                label={"Title"}
                color={"secondary"}
                inputValue={_get(formState, `${formKey}.title`, "")}
                isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "title", true)}
                errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "title")}
                onChangeFn={(event) => onChangeFn(event)}
            />

            <InputDateTimePicker
                inputName={"date_time"}
                isRequired={true}
                isInputLabelProps={true}
                isInputProps={true}
                isInputAttributesProps={true}
                isWhiteLabel={true}
                label={"Date & Time"}
                color={"secondary"}
                helperText={"Please click the calendar icon"}
                inputValue={_get(formState, `${formKey}.date_time`, new Date())}
                isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "date_time", true)}
                errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "date_time")}
                onChangeFn={(event) => onChangeFn(event)}
            />

            <CustomInputField
                inputName={"description"}
                isRequired={true}
                label={"Description"}
                color={"secondary"}
                isMultiline={true}
                rows={6}
                inputValue={_get(formState, `${formKey}.description`, "")}
                isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "description", true)}
                errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "description")}
                onChangeFn={(event) => onChangeFn(event)}
            />

            <InputButton
                elementWrapperStyle={"add-todo-btn"}
                btnText={"Create New Task"}
                color={"secondary"}
                onClickFn={onClickBtnFn}
            />
        </Fragment>
    );
};

export default AddToDo;