import React, { Fragment, useContext, useState } from "react";
import InputToggleBtnGroup from "../../../../ui-elements/InputToggleBtnGroup";
import Button from "@mui/material/Button";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

import { UICard } from "../../../../ui-elements/BaseElements";
import DialogPopup from "../../../../ui-elements/DialogPopup";
import { UIContext } from "../../../../providers/context-providers/UIContext.provider";
import { callApi } from "../../../../../helpers/callApi.helpers";
import { todoAPI } from "../../../../../config/apiUrl.config";
import { DeleteToDoPopup, EditToDoPopup, ViewToDoPopup } from "./ToDoPopups";
import { FormContext } from "../../../../providers/context-providers/FormContext.provider";
import validate from "../../../../../helpers/validation";
import { _get } from "../../../../../helpers/lodash.wrappers";

const emptyFn = (...para) => undefined;

const ToDo = ({
    id = "",
    title = "",
    description = "",
    dateTime = "",
    status = "",
    getTodosFn = emptyFn
}) => {
    const [, uiAction] = useContext(UIContext);
    const [formState, formAction] = useContext(FormContext);
    const [anchorEl, setAnchorEl] = useState(null);
    const [dialogOpen, setDialogOpen] = useState({
        status: false,
        type: null
    });

    const open = Boolean(anchorEl);
    const dataList = [
        {
            label: "Todo",
            value: "TO_DO"
        },
        {
            label: "Inprogress",
            value: "IN_PROGRESS"
        },
        {
            label: "Done",
            value: "DONE"
        }
    ];

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (type) => {
        setAnchorEl(null);
        setDialogOpen({ status: true, type });
    };

    const getDateWithFormat = (value) => {
        const dateObject = new Date(value);
        return `${dateObject.getFullYear()}-${(dateObject.getMonth() + 1).toString().padStart(2, '0')}-${dateObject.getDate().toString().padStart(2, '0')} ${dateObject.getHours().toString().padStart(2, '0')}:${dateObject.getMinutes().toString().padStart(2, '0')}`;
    };

    const updateStatus = (statusValue) => {
        if (statusValue) {
            uiAction.setPageLoader(true);

            callApi(`${todoAPI}/status/${id}`)
                .headers(true)
                .isMultipart(false)
                .method("patch")
                .body({
                    "status": statusValue
                })
                .send((error, result) => {
                    if (error) {
                        uiAction.setPageLoader(false);
                        uiAction.setFlashMessage({
                            status: true,
                            message: "An error has occurred",
                            messageType: "error"
                        });
                    } else {
                        getTodosFn();
                        uiAction.setFlashMessage({
                            status: true,
                            message: "Status has successfully updated",
                            messageType: "success"
                        });
                    }
                });
        }
    };

    const deleteTask = () => {
        uiAction.setPageLoader(true);

        callApi(`${todoAPI}/${id}`)
            .headers(true)
            .isMultipart(false)
            .method("delete")
            .body({})
            .send((error, result) => {
                if (error) {
                    uiAction.setPageLoader(false);
                    uiAction.setFlashMessage({
                        status: true,
                        message: "An error has occurred",
                        messageType: "error"
                    });
                } else {
                    setDialogOpen({ status: false, type: null });
                    getTodosFn();
                    uiAction.setFlashMessage({
                        status: true,
                        message: "Task has successfully deleted",
                        messageType: "success"
                    });
                }
            });
    };

    const updateTask = () => {
        const editFormKey = "edit_todo_form";
        let validationErrors = true;

        validate(_get(formState, editFormKey, {}))
            .setFields({
                "title": "Title",
                "description": "Description",
                "date_time": "Date & Time",
            })
            .setRules({
                "title": "required|max:50",
                "description": "required|max:255",
                "date_time": "required",
            })
            .run((result) => {
                if (result) {
                    formAction.setFormErrorFn(editFormKey, result);
                    validationErrors = true;
                } else {
                    formAction.setFormErrorFn(editFormKey, []);
                    validationErrors = false;
                }
            });

        if (!validationErrors) {
            uiAction.setPageLoader(true);

            callApi(`${todoAPI}/${id}`)
                .headers(true)
                .isMultipart(false)
                .method("put")
                .body({
                    "title": _get(formState, `${editFormKey}.title`, ""),
                    "description": _get(formState, `${editFormKey}.description`, ""),
                    "date_time": _get(formState, `${editFormKey}.date_time`, new Date()).toISOString(),
                    "status": _get(formState, `${editFormKey}.status`, "")
                })
                .send((error, result) => {
                    if (error) {
                        uiAction.setPageLoader(false);

                        if (_get(error, "data.errors.name", "") === "Bad Request") {
                            formAction.setFormErrorFn(editFormKey, _get(error, "data.errors.details", []));
                        } else {
                            uiAction.setFlashMessage({
                                status: true,
                                message: "An error has occurred",
                                messageType: "error"
                            });
                        }
                    } else {
                        setDialogOpen({ status: false, type: null });
                        getTodosFn();
                        uiAction.setFlashMessage({
                            status: true,
                            message: "Task has successfully updated",
                            messageType: "success"
                        });
                    }
                });
        }
    };

    const maskTitle = (value = "") => {
        if (value.length >= 26) {
            return `${value.slice(0, 26)}...`;
        } else {
            return value;
        }
    };

    return (
        <Fragment>
            <UICard elementWrapperStyle={"mb-2"}>
                <div className={"row todo-container"}>
                    <div className={"col-sm-5"}>
                        <p className={"mb-0 todo-des"}>{maskTitle(title)}</p>
                        <p className={"mb-0 todo-date"}>{getDateWithFormat(dateTime)}</p>
                    </div>

                    <div className={"col-sm-5"}>
                        <InputToggleBtnGroup
                            inputValue={status}
                            dataList={dataList}
                            onChangeFn={(event) => updateStatus(event.value)}
                        />
                    </div>

                    <div className={"col-sm-2 todo-actions"}>
                        <Button
                            variant="outlined"
                            endIcon={<ArrowDropDownIcon />}
                            id="basic-button"
                            aria-controls={open ? 'basic-menu' : undefined}
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}
                        >
                            More
                        </Button>

                        <Menu
                            id="basic-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            MenuListProps={{
                                'aria-labelledby': 'basic-button',
                            }}
                        >
                            <MenuItem onClick={() => handleClose("view")}>View</MenuItem>
                            <MenuItem onClick={() => handleClose("edit")}>Edit</MenuItem>
                            <MenuItem onClick={() => handleClose("delete")}>Delete</MenuItem>
                        </Menu>
                    </div>
                </div>
            </UICard>

            <DialogPopup
                isOpen={dialogOpen.status}
                isFullWidth={true}
                maxWidth={"sm"}
                dialogTitle={dialogOpen.type === "view" ? "View Task" : dialogOpen.type === "edit" ? "Edit Task" : "Delete Task"}
                onClosePopupFn={() => setDialogOpen({ status: false, type: null })}
                isSaveButton={dialogOpen.type !== "view"}
                closeBtnText={dialogOpen.type === "delete" ? "No" : "Close"}
                saveBtnText={dialogOpen.type === "delete" ? "Yes" : "Update"}
                onSavePopupFn={() => {
                    if (dialogOpen.type === "delete") {
                        deleteTask();
                    } else if (dialogOpen.type === "edit") {
                        updateTask();
                    }
                }}
            >
                {
                    (dialogOpen.type === "view") ? (
                        <ViewToDoPopup title={title} description={description} dateTime={dateTime} status={status} />
                    ) : (
                        (dialogOpen.type === "edit") ? (
                            <EditToDoPopup title={title} description={description} dateTime={dateTime} status={status} />
                        ) : (
                            <DeleteToDoPopup  />
                        )
                    )
                }
            </DialogPopup>
        </Fragment>
    );
};

export default ToDo;