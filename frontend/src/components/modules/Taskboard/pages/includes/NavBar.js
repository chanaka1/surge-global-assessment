import React, { useContext } from "react";
import { AppBar, Toolbar } from "@mui/material";
import InputButton from "../../../../ui-elements/InputButton";

import { AuthContext } from "../../../../providers/context-providers/AuthContext.provider";

const NavBar = () => {
    const [, authAction] = useContext(AuthContext);

    return (
        <AppBar position="static" color={"secondary"}>
            <Toolbar>
                <div className={"navbar-wrapper"}>
                    <div className={"navbar-left-wrapper"}>
                        <div>
                            <i className="fas fa-tasks"></i>
                        </div>

                        <div className={"app-name"}>
                            <h5>Todo App</h5>
                        </div>
                    </div>

                    <div className={"navbar-right-wrapper"}>
                        <InputButton
                            elementWrapperStyle={"navbar-btn"}
                            btnText={"Log Out"}
                            isFullWidth={false}
                            variant={"text"}
                            onClickFn={() => authAction.unauthorisedUserFn()}
                        />
                    </div>
                </div>
            </Toolbar>
        </AppBar>
    );
};

export default NavBar;