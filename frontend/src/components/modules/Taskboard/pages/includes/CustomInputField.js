import React from "react";
import { withStyles } from "@mui/styles";

import InputField from "../../../../ui-elements/InputField";

const styles = theme => ({
    cssLabel: {
        color : '#FFFFFF'
    },
    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: `#FFFFFF !important`,
        }
    },
    cssFocused: {},
    notchedOutline: {
        borderWidth: '2px',
        borderColor: '#FFFFFF !important'
    },
});

const emptyFn = (...para) => undefined;

const CustomInputField = ({
    classes,
    elementStyle = "",
    color = "primary",
    inputType = "text",
    inputName = "",
    isDisabled = false,
    isError = false,
    isFullWidth = true,
    label = "",
    maxRows = null,
    minRows = null,
    isMultiline = false,
    placeholder = "",
    isRequired = false,
    rows = 1,
    inputValue = "",
    variant = "outlined",
    size = "small",
    helperText = "",
    errorText = "",
    onChangeFn = emptyFn,
}) => {
    return (
        <InputField
            elementStyle={elementStyle}
            color={color}
            inputType={inputType}
            inputName={inputName}
            isDisabled={isDisabled}
            isError={isError}
            isFullWidth={isFullWidth}
            label={label}
            maxRows={maxRows}
            minRows={minRows}
            isMultiline={isMultiline}
            placeholder={placeholder}
            rows={rows}
            isRequired={isRequired}
            inputValue={inputValue}
            variant={variant}
            size={size}
            helperText={helperText}
            errorText={errorText}
            onChangeFn={onChangeFn}
            inputLabelProps={{
                classes: {
                    root: classes.cssLabel,
                    focused: classes.cssFocused,
                },
            }}
            inputProps={{
                classes: {
                    root: classes.cssOutlinedInput,
                    focused: classes.cssFocused,
                    notchedOutline: classes.notchedOutline,
                }
            }}
            inputAttributesProps={{ style: { color: '#FFFFFF' } }}
        />
    );
};

export default withStyles(styles)(CustomInputField);