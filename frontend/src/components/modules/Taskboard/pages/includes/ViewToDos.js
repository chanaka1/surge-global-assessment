import React, { Fragment, useContext } from "react";

import ToDo from "./ToDo";
import { FormContext } from "../../../../providers/context-providers/FormContext.provider";
import { _get } from "../../../../../helpers/lodash.wrappers";

const ViewToDos = (props) => {
    const [formState] = useContext(FormContext);
    const todoList = _get(formState, "dashboard_form.todos", []);

    return (
        <Fragment>
            <div className={"mt-3 mb-4 view-todo-heading"}>
                <i className="far fa-sticky-note"></i>
                <h5>All Tasks</h5>
            </div>

            {
                (todoList.length === 0) ? (
                    <div>You don't have any task to do</div>
                ) : (
                    todoList.map((value, index) => {
                        return (
                            <ToDo
                                key={index}
                                id={value._id}
                                title={value.title}
                                description={value.description}
                                status={value.status}
                                dateTime={value.date_time}
                                getTodosFn={props.getTodosFn}
                            />
                        );
                    })
                )
            }
        </Fragment>
    );
};

export default ViewToDos;