import React, { Fragment, useContext, useEffect } from "react";

import NavBar from "./includes/NavBar";
import AddToDo from "./includes/AddToDo";
import ViewToDos from "./includes/ViewToDos";
import PageLoader from "../../../ui-elements/PageLoader";
import { SnackBarList } from "../../../ui-elements/SnackbarWrapper";
import { FormContext } from "../../../providers/context-providers/FormContext.provider";
import { UIContext } from "../../../providers/context-providers/UIContext.provider";
import { callApi } from "../../../../helpers/callApi.helpers";
import { todoAPI } from "../../../../config/apiUrl.config";
import { _get } from "../../../../helpers/lodash.wrappers";

const TaskBoard = () => {
    const [, formAction] = useContext(FormContext);
    const [, uiAction] = useContext(UIContext);
    const formKey = "dashboard_form";

    useEffect(() => {
        formAction.initFromFn({ [formKey]: {} });
        getTodos();

        return () => {
            formAction.removeFromGroupFn(formKey);
        };
    }, []);

    const getTodos = () => {
        uiAction.setPageLoader(true);

        callApi(`${todoAPI}/list`)
            .headers(true)
            .isMultipart(false)
            .method("get")
            .body({})
            .send((error, result) => {
                uiAction.setPageLoader(false);

                if (!error) {
                    formAction.changeInputFn(formKey, "todos", _get(result, "data.data", []));
                }
            });
    };

    return (
        <Fragment>
            <NavBar />

            <div className={"container-fluid"}>
                <div className={"row"}>
                    <div className={"col-md-6 add-todo-wrapper"}>
                        <AddToDo getTodosFn={getTodos} />
                    </div>

                    <div className={"col-md-6 view-todo-wrapper"}>
                        <ViewToDos getTodosFn={getTodos} />
                    </div>
                </div>
            </div>

            <PageLoader />
            <SnackBarList />
        </Fragment>
    );
};

export default TaskBoard;