import React, { Fragment } from "react";

import AuthRouteGuard from "../../middlewares/AuthRouteGuard";
import TaskBoard from "./pages/TaskBoard";

const TaskBoardRoutes = () => {
    return (
        <Fragment>
            <AuthRouteGuard exact={true} isAuthProtected={true} path={"/"} component={TaskBoard} />
        </Fragment>
    );
};

export default TaskBoardRoutes;