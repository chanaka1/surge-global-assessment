import React, { useContext, useEffect } from "react";
import { Card, CardContent } from "@mui/material";
import { Link } from "react-router-dom";

import { _get } from "../../../../helpers/lodash.wrappers";
import InputField from "../../../ui-elements/InputField";
import InputButton from "../../../ui-elements/InputButton";
import { FormContext } from "../../../providers/context-providers/FormContext.provider";
import { UIContext } from "../../../providers/context-providers/UIContext.provider";
import { getInputErrors } from "../../../../helpers/common.helpers";
import validate from "../../../../helpers/validation";
import { callApi } from "../../../../helpers/callApi.helpers";
import { authAPI } from "../../../../config/apiUrl.config";
import { clientId, clientSecret } from "../../../../config/core.config";
import PageLoader from "../../../ui-elements/PageLoader";
import { SnackBarList } from "../../../ui-elements/SnackbarWrapper";
import { AuthContext } from "../../../providers/context-providers/AuthContext.provider";
import { setAuthData } from "../../../../helpers/manageStorage.helpers";

const Login = () => {
    const [formState, formAction] = useContext(FormContext);
    const [, uiAction] = useContext(UIContext);
    const [, authAction] = useContext(AuthContext);
    const formKey = "login_form";

    useEffect(() => {
        formAction.initFromFn({ [formKey]: {} });

        return () => {
            formAction.removeFromGroupFn(formKey);
        };
    }, []);

    const onChangeFn = (event) => {
        formAction.changeInputFn(formKey, event.name, event.value);
    };

    const onClickBtnFn = () => {
        let validationErrors = true;

        validate(_get(formState, formKey, {}))
            .setFields({
                "username": "Username",
                "password": "Password"
            })
            .setRules({
                "username": "required",
                "password": "required"
            })
            .run((result) => {
                if (result) {
                    formAction.setFormErrorFn(formKey, result);
                    validationErrors = true;
                } else {
                    formAction.setFormErrorFn(formKey, []);
                    validationErrors = false;
                }
            });

        if (!validationErrors) {
            uiAction.setPageLoader(true);

            callApi(authAPI)
                .headers(false)
                .isMultipart(false)
                .method("post")
                .body({
                    "username": _get(formState, `${formKey}.username`, ""),
                    "password": _get(formState, `${formKey}.password`, ""),
                    "client_id": clientId,
                    "client_secret": clientSecret
                })
                .send((error, result) => {
                    uiAction.setPageLoader(false);

                    if (error) {
                        if (_get(error, "data.errors.name", "") === "Unauthorized") {
                            const errors = [
                                {
                                    "property": "username",
                                    "message": "Username or Password is incorrect"
                                },
                                {
                                    "property": "password",
                                    "message": "Username or Password is incorrect"
                                }
                            ];

                            formAction.setFormErrorFn(formKey, errors);
                        } else {
                            uiAction.setFlashMessage({
                                status: true,
                                message: "An error has occurred",
                                messageType: "error"
                            });
                        }
                    } else {
                        const authObj = {
                            accessToken: _get(result, "data.access_token", ""),
                            refreshToken: _get(result, "data.refresh_token", "")
                        };

                        setAuthData(authObj);
                        authAction.setTokensFn(authObj);
                    }
                });
        }
    };

    return (
        <div className={"login-container"}>
            <Card className={"login-wrapper"}>
                <div className={"login-header"}>
                    <div className={"text-primary text-center p-4"}>
                        <h5 className={"text-white font-size-20"}>
                            Welcome Back !
                        </h5>

                        <p className={"text-white-50"}>
                            Log into To-Do List App
                        </p>

                        <i className="fas fa-tasks"></i>
                        {/*<img src={"images/logo.png"} height="50" alt="logo" />*/}
                    </div>
                </div>

                <CardContent className={"pb-0"}>
                    <InputField
                        inputName={"username"}
                        isRequired={true}
                        label={"Username"}
                        inputValue={_get(formState, `${formKey}.username`, "")}
                        isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "username", true)}
                        errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "username")}
                        onChangeFn={(event) => onChangeFn(event)}
                    />

                    <InputField
                        inputName={"password"}
                        isRequired={true}
                        label={"Password"}
                        inputType={"password"}
                        inputValue={_get(formState, `${formKey}.password`, "")}
                        isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "password", true)}
                        errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "password")}
                        onChangeFn={(event) => onChangeFn(event)}
                    />

                    <InputButton
                        btnText={"Log In"}
                        isFullWidth={true}
                        onClickFn={onClickBtnFn}
                    />
                </CardContent>

                <div className={"register-link text-center"}>
                    <Link to={"/register"}>Click here to register</Link>
                </div>
            </Card>

            <PageLoader />
            <SnackBarList />
        </div>
    );
};

export default Login;