import React, { Fragment } from "react";

import Login from "./pages/Login";
import AuthRouteGuard from "../../middlewares/AuthRouteGuard";

const LoginRoutes = () => {
    return (
        <Fragment>
            <AuthRouteGuard exact={true} isAuthProtected={false} path={"/login"} component={Login} />
        </Fragment>
    );
};

export default LoginRoutes;