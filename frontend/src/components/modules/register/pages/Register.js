import React, { useContext, useEffect } from "react";
import { Card, CardContent } from "@mui/material";
import { Link } from "react-router-dom";

import InputField from "../../../ui-elements/InputField";
import InputButton from "../../../ui-elements/InputButton";
import { FormContext } from "../../../providers/context-providers/FormContext.provider";
import { _get } from "../../../../helpers/lodash.wrappers";
import validate from "../../../../helpers/validation";
import { getDataByFormObject, getInputErrors } from "../../../../helpers/common.helpers";
import { callApi } from "../../../../helpers/callApi.helpers";
import { usersAPI } from "../../../../config/apiUrl.config";
import PageLoader from "../../../ui-elements/PageLoader";
import { SnackBarList } from "../../../ui-elements/SnackbarWrapper";
import { UIContext } from "../../../providers/context-providers/UIContext.provider";

const Register = (props) => {
    const [formState, formAction] = useContext(FormContext);
    const [, uiAction] = useContext(UIContext);
    const formKey = "register_form";

    useEffect(() => {
        formAction.initFromFn({ [formKey]: {} });

        return () => {
            formAction.removeFromGroupFn(formKey);
        };
    }, []);

    const onChangeFn = (event) => {
        formAction.changeInputFn(formKey, event.name, event.value);
    };

    const onClickBtnFn = () => {
        let validationErrors = true;

        validate(_get(formState, formKey, {}))
            .setFields({
                "full_name": "Full Name",
                "email": "Email",
                "username": "Username",
                "password": "Password",
                "confirm_password": "Confirm Password"
            })
            .setRules({
                "full_name": "required|max:100",
                "email": "required|email|max:50",
                "username": "required|max:50",
                "password": "required",
                "confirm_password": "required|same:password"
            })
            .run((result) => {
                if (result) {
                    formAction.setFormErrorFn(formKey, result);
                    validationErrors = true;
                } else {
                    formAction.setFormErrorFn(formKey, []);
                    validationErrors = false;
                }
            });

        if (!validationErrors) {
            uiAction.setPageLoader(true);

            callApi(usersAPI)
                .headers(false)
                .isMultipart(false)
                .method("post")
                .body(getDataByFormObject(_get(formState, formKey, {})))
                .send((error, result) => {
                    uiAction.setPageLoader(false);

                    if (error) {
                        if (_get(error, "data.errors.name", "") === "Bad Request") {
                            formAction.setFormErrorFn(formKey, _get(error, "data.errors.details", []));
                        } else {
                            uiAction.setFlashMessage({
                                status: true,
                                message: "An error has occurred",
                                messageType: "error"
                            });
                        }
                    } else {
                        uiAction.setFlashMessage({
                            status: true,
                            message: "You have successfully registered",
                            messageType: "success"
                        });

                        setTimeout(() => {
                            props.history.push("/");
                        }, 1000);
                    }
                });
        }
    };

    return (
        <div className={"login-container"}>
            <Card className={"login-wrapper"}>
                <div className={"login-header"}>
                    <div className={"text-primary text-center p-4"}>
                        <h5 className={"text-white font-size-20"}>
                            Welcome Back !
                        </h5>

                        <p className={"text-white-50"}>
                            Sign upto To-Do List App
                        </p>

                        <i className="fas fa-tasks"></i>
                        {/*<img src={"images/logo.png"} height="50" alt="logo" />*/}
                    </div>
                </div>

                <CardContent className={"pb-0"}>
                    <InputField
                        inputName={"full_name"}
                        isRequired={true}
                        label={"Full Name"}
                        inputValue={_get(formState, `${formKey}.full_name`, "")}
                        isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "full_name", true)}
                        errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "full_name")}
                        onChangeFn={(event) => onChangeFn(event)}
                    />

                    <InputField
                        inputName={"email"}
                        isRequired={true}
                        label={"Email"}
                        inputValue={_get(formState, `${formKey}.email`, "")}
                        isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "email", true)}
                        errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "email")}
                        onChangeFn={(event) => onChangeFn(event)}
                    />

                    <InputField
                        inputName={"username"}
                        isRequired={true}
                        label={"Username"}
                        inputValue={_get(formState, `${formKey}.username`, "")}
                        isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "username", true)}
                        errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "username")}
                        onChangeFn={(event) => onChangeFn(event)}
                    />

                    <InputField
                        inputName={"password"}
                        isRequired={true}
                        label={"Password"}
                        inputType={"password"}
                        inputValue={_get(formState, `${formKey}.password`, "")}
                        isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "password", true)}
                        errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "password")}
                        onChangeFn={(event) => onChangeFn(event)}
                    />

                    <InputField
                        inputName={"confirm_password"}
                        isRequired={true}
                        label={"Confirm Password"}
                        inputType={"password"}
                        inputValue={_get(formState, `${formKey}.confirm_password`, "")}
                        isError={getInputErrors(_get(formState, `${formKey}._errors`, []), "confirm_password", true)}
                        errorText={getInputErrors(_get(formState, `${formKey}._errors`, []), "confirm_password")}
                        onChangeFn={(event) => onChangeFn(event)}
                    />

                    <InputButton
                        btnText={"Sign Up"}
                        isFullWidth={true}
                        onClickFn={onClickBtnFn}
                    />
                </CardContent>

                <div className={"register-link text-center"}>
                    <Link to={"/login"}>Already have an account?</Link>
                </div>
            </Card>

            <PageLoader />
            <SnackBarList />
        </div>
    );
};

export default Register;