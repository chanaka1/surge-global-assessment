import React, { Fragment } from "react";

import Register from "./pages/Register";
import AuthRouteGuard from "../../middlewares/AuthRouteGuard";

const RegisterRoutes = () => {
    return (
        <Fragment>
            <AuthRouteGuard exact={true} isAuthProtected={false} path={"/register"} component={Register} />
        </Fragment>
    );
};

export default RegisterRoutes;