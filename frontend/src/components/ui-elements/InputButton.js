import React from "react";
import Button from '@mui/material/Button';

const emptyFn = (...para) => undefined;

const InputButton = ({
    elementWrapperStyle = "",
    variant = "contained",
    btnText = "",
    color = "primary",
    isDisabled = false,
    isFullWidth = true,
    size = "medium",
    onClickFn = emptyFn
}) => {
    return (
        <div className={elementWrapperStyle}>
            <Button
                sx={{ textTransform: "none" }}
                variant={variant}
                color={color}
                disabled={isDisabled}
                fullWidth={isFullWidth}
                size={size}
                onClick={() => onClickFn()}
            >
                {btnText}
            </Button>
        </div>
    );
};

export default InputButton;