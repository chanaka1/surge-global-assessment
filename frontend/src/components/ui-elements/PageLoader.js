import React, { useContext, Fragment } from 'react';
import CircularProgress from '@mui/material/CircularProgress';

import { UIContext } from "../providers/context-providers/UIContext.provider";

const PageLoader = () => {
    const [state] = useContext(UIContext);

    return (
        <Fragment>
            {
                (state.setPageLoader===true) ? (
                    <div className="pageLoader">
                        <CircularProgress
                            className={"loader"}
                            size={50}
                            thickness={3}
                        />
                    </div>
                ) : null
            }
        </Fragment>
    );
};

export default PageLoader;