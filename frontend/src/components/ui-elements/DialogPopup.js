import React from "react";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    Slide,
    AppBar,
    Toolbar
} from "@mui/material";

const emptyFun = (...para) => undefined;

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const DialogPopup = ({
    isFullScreen = false,
    isOpen = false,
    isFullWidth = false,
    maxWidth = "sm",
    children = null,
    dialogTitle = "",
    isCloseButton = true,
    isSaveButton = true,
    closeBtnText = "Close",
    saveBtnText = "Save",
    onClosePopupFn = emptyFun,
    onSavePopupFn = emptyFun,
}) => {
    return (
        <Dialog
            fullScreen={isFullScreen}
            open={isOpen}
            fullWidth={isFullWidth}
            maxWidth={maxWidth}
            TransitionComponent={Transition}
            keepMounted
            onClose={onClosePopupFn}
        >
            <AppBar position="static" color={"primary"}>
                <Toolbar>
                    <h5>{dialogTitle}</h5>
                </Toolbar>
            </AppBar>

            <DialogContent>
                {children}
            </DialogContent>

            <DialogActions>
                {
                    (isCloseButton) ? (
                        <Button
                            variant={"outlined"}
                            onClick={onClosePopupFn}
                            sx={{ textTransform: "none" }}
                        >
                            {closeBtnText}
                        </Button>
                    ) : null
                }

                {
                    (isSaveButton) ? (
                        <Button
                            variant={"outlined"}
                            onClick={onSavePopupFn}
                            sx={{ textTransform: "none" }}
                        >
                            {saveBtnText}
                        </Button>
                    ) : null
                }
            </DialogActions>
        </Dialog>
    );
};

export default DialogPopup;