import React from "react";
import { FormControl, TextField } from "@mui/material";
import { withStyles } from "@mui/styles";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateTimePicker from '@mui/lab/DateTimePicker';

const emptyFn = (...para) => undefined;

const styles = theme => ({
    cssLabel: {
        color : '#FFFFFF'
    },
    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: `#FFFFFF !important`,
        }
    },
    cssFocused: {},
    notchedOutline: {
        borderWidth: '2px',
        borderColor: '#FFFFFF !important'
    },
});

const InputDateTimePicker = ({
    classes,
    elementStyle = "",
    color = "primary",
    inputName = "",
    isDisabled = false,
    isFullWidth = true,
    label = "",
    isRequired = false,
    isInputLabelProps = false,
    isInputProps = false,
    isInputAttributesProps = false,
    isWhiteLabel = false,
    inputValue = new Date(),
    variant = "outlined",
    helperText = "",
    errorText = "",
    onChangeFn = emptyFn,
}) => {
    return (
        <div className={`date-time-wrapper ${isWhiteLabel ? "white-label" : ""} ${elementStyle}`}>
            <FormControl
                fullWidth={isFullWidth}
            >
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DateTimePicker
                        color={color}
                        name={inputName}
                        disabled={isDisabled}
                        required={isRequired}
                        label={label}
                        value={inputValue}
                        variant={variant}
                        InputLabelProps={(isInputLabelProps ? {
                            classes: {
                                root: classes.cssLabel,
                                focused: classes.cssFocused,
                            },
                        } : {})}
                        InputProps={(isInputProps ? {
                            classes: {
                                root: classes.cssOutlinedInput,
                                focused: classes.cssFocused,
                                notchedOutline: classes.notchedOutline,
                            },
                            readOnly: true
                        } : {})}
                        inputProps={isInputAttributesProps ? { style: { color: '#FFFFFF' } } : {}}
                        onChange={(event) => {
                            onChangeFn({
                                name: inputName,
                                value: event
                            })
                        }}
                        renderInput={(params) => <TextField {...params} />}
                    />

                    <p className={`input-date-time-helper-text`}>
                        {helperText}
                    </p>
                </LocalizationProvider>
            </FormControl>
        </div>
    )
};

export default withStyles(styles)(InputDateTimePicker);