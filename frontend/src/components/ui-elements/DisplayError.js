import React from "react";
import { Link } from "react-router-dom";

const DisplayError = () => {
    const reload = () => {
        setTimeout(() => {
            window.location.reload()
        }, 100)
    };

    return (
        <div className={"container-fluid error-overlay"}>

            <div className={"row justify-content-center"}>

                <div className={"col-md-6"}>

                    <div className={"card mt-4"}>
                        <div className={"card-body text-center"}>
                            <h1>Page is under constructing</h1>

                            <Link to={"/"} onClick={reload}>
                                <h6 className={"mt-4"}>Go back to:- Dashboard</h6>
                            </Link>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    );
};

export default DisplayError;
