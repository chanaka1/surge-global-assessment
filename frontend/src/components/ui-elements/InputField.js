import React from "react";
import TextField from '@mui/material/TextField';

const emptyFn = (...para) => undefined;

const InputField = ({
    elementStyle = "",
    color = "primary",
    inputType = "text",
    inputName = "",
    isDisabled = false,
    isError = false,
    isFullWidth = true,
    label = "",
    maxRows = null,
    minRows = null,
    isMultiline = false,
    placeholder = "",
    isRequired = false,
    rows = 1,
    inputValue = "",
    variant = "outlined",
    size = "small",
    helperText = "",
    errorText = "",
    inputLabelProps = {},
    inputProps = {},
    inputAttributesProps = {},
    onChangeFn = emptyFn,
}) => {
    return (
        <div className={`input-field-wrapper ${(helperText || errorText) ? "input-field-wrapper-margin" : ""} ${elementStyle}`}>
            <TextField
                color={color}
                type={inputType}
                name={inputName}
                disabled={isDisabled}
                error={isError}
                fullWidth={isFullWidth}
                label={label}
                maxRows={maxRows}
                minRows={minRows}
                multiline={isMultiline}
                placeholder={placeholder}
                required={isRequired}
                rows={rows}
                value={inputValue}
                variant={variant}
                size={size}
                InputLabelProps={inputLabelProps}
                InputProps={inputProps}
                inputProps={inputAttributesProps}
                onChange={(event) => {
                    onChangeFn({
                        name: inputName,
                        value: event.target.value
                    })
                }}
            />

            <p className={`input-field-helper-text ${isError ? "input-error" : ""}`}>
                {isError ? errorText : helperText}
            </p>
        </div>
    );
};

export default InputField;