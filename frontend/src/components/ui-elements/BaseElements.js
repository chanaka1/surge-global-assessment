import React, { Fragment } from "react";
import {
    createTheme,
    CssBaseline,
    ThemeProvider,
    Card,
    CardContent
} from "@mui/material";

import { baseTemplate } from "../../config/template.config";

const MaterialThemeProvider = ({ children = null }) => {
    return (
        <Fragment>
            <CssBaseline />
            <ThemeProvider theme={createTheme(baseTemplate)}>
                {children}
            </ThemeProvider>
        </Fragment>
    );
};

const UICard = ({
    elementWrapperStyle = "",
    cardStyle = "",
    cardContentStyle = "",
    children = null
}) => {
    return (
        <div className={elementWrapperStyle}>
            <Card className={cardStyle}>
                <CardContent className={cardContentStyle}>
                    {children}
                </CardContent>
            </Card>
        </div>
    );
};

export {
    MaterialThemeProvider,
    UICard
};