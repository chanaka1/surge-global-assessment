import React from "react";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";

const emptyFn = (...para) => undefined;

const InputToggleBtnGroup = ({
    elementWrapperStyle = "",
    color = "primary",
    inputValue = "",
    dataList = [],
    onChangeFn = emptyFn
}) => {
  return (
      <div className={`toggle-btn-grp ${elementWrapperStyle}`}>
          <ToggleButtonGroup
              color={color}
              value={inputValue}
              exclusive
              onChange={(event, value) => onChangeFn({ event, value })}
          >
              {
                  dataList.map((value, index) => {
                      return (
                          <ToggleButton key={index} value={value["value"]}>{value["label"]}</ToggleButton>
                      );
                  })
              }
          </ToggleButtonGroup>
      </div>
  );
};

export default InputToggleBtnGroup;