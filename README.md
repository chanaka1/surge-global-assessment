# Surge Global Assessment



## Prerequisites

* Install the NodeJs (Better if you can install NodeJs version greater than or equal to 14).
* Install MongoDB server (Optional, because I have pointed the backend server to a MongoDB Atlas cloud).

## Backend

* Backend is developed using the NodeJs (ExpressJs framework).
* In the project directory, go the backend folder.
* No need to provide your DB details, because I have pointed the backend server to a MongoDB Atlas cloud.
* Backend will run on port 5000, if you want to change this, you can change the APP_PORT number in the .env file (If you change the port number, you have to modifiy the frontend .env file as well).
* After done above points, open a CMD from inside the backend folder.
* Type "npm install" (Without quotes) and hit enter.
* After the completion of the npm install, type "npm start" (Without quotes) in the CMD and hit enter.
* Backend server will run on 5000 port (Default).

## Frontend

* Frontend is developed using ReactJs.
* In the project directory, go the frontend folder.
* If you have changed the default running port number of the backend, modify the REACT_APP_API_BASE_URL variable in the frontend .env file.
* Open a CMD from inside the frontend folder.
* Type "npm install" (Without quotes) and hit enter.
* After the completion of the npm install, type "npm start" (Without quotes) in the CMD and hit enter.
* Frontend will run on port 3000.
* Usually, your default browser will automatically open a tab for the frontend application, if not go to http://localhost:3000/ in your browser to open the frontend application.

** Please Note:- You can create a user account by navigating to the register UI, just if case you need a valid user account, please use below credentials. **

* Username: chanaka@gmail.com
* Password: Admin@123

** Please Note:- If you are willing to connect backend server to your local database, you have to do the necessary modifications in .env, database.config.js, server.js files (in backend directory). After that you have to manually create collection name "oauthclients". Please insert the data in "oauth-clients-details.txt" file (in backend folder) to the "oauthclients" collection.
